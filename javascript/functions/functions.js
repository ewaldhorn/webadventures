const myFunc = (a,bob) => a+bob;

function myFunc2(all,b) {
  return all+b;
}

const myFunc3 = (lola, molly) => {
  const result = molly - lola;
  console.log(result);
  return result;
}

const arr = [1,4,5,3,1,291,2,1];

const MyCompareFunc = (oompa, loompa) => oompa - loompa;

console.log(arr.sort(myFunc3));
console.log(arr.sort((x,y)=>y-x));
console.log(arr.sort(MyCompareFunc));
console.log(arr.sort((wq,kq)=>MyCompareFunc(kq,wq)));
console.log(arr.sort(function (aa,bb){return aa-bb;}));

//console.log(myFunc(10,20));
//console.log(myFunc2(20,10));

console.log("This is crazy!", MyCompareFunc(10,20));
